package com.metlife.starteam;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

public class ProjectAnalyzerUtil {
	
	public static List<File> filterFiles (List<File> files, String fileExtn) {
		
		for (int fileCount = 0; fileCount <files.size();  fileCount++) {
			if (FilenameUtils.isExtension(files.get(fileCount).getName(), fileExtn)) {
				files.remove(fileCount);
			}
		}
		
		return files;
	}
	
	public static List<JarDetails> captureJarDetails (List<File> jars) throws IOException {
		
		List<JarDetails> jarDetailsList = new ArrayList<JarDetails>();
		
		for (File jar : jars) {
			JarDetails jarDetails = new JarDetails();
			jarDetails.setJarName(jar.getName());
			jarDetails.setJarVersion(new javaxt.io.Jar(jar).getVersion());
			jarDetails.setJarLocation(jar.getCanonicalPath());
			jarDetails.setJarChecksum(ChecksumCalculator.calculateChecksum(jar));
			jarDetailsList.add(jarDetails);
		}
		
		return jarDetailsList;
	}
}
