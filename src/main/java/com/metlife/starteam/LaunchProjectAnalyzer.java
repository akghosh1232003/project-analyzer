package com.metlife.starteam;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class LaunchProjectAnalyzer {

	public static void main(String[] args) throws IOException {
		//Take the project root folder path and start analyze
		String rootFolder = "/Users/anupghosh/Documents/workspace-sts/FileUploadForm";
		
		ProjectAnalyzer projectAnalyzer = new ProjectAnalyzer();
		List<File> filesList = projectAnalyzer.analyzeProject(rootFolder);
		List<File> jarFilesList = projectAnalyzer.analyzeProjectJars(rootFolder);
		List<JarDetails> jarDetailsList = ProjectAnalyzerUtil.captureJarDetails(jarFilesList);
		
		ExcelReportGenerator reportGenerator = new ExcelReportGenerator();
		reportGenerator.generateExcelReport(filesList, jarDetailsList);
	}
}
