package com.metlife.starteam;

import java.io.File;
import java.util.List;

public interface IProjectAnalyzer {
	
	public List<File> analyzeProject (String rootFolderName);
		
	public List<File> analyzeProjectJars (String jarName);
		
}
