package com.metlife.starteam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class ExcelReportGenerator {
	
	public void generateExcelReport (List<File> files, List <JarDetails> jarDetails) {
		
		FileInputStream fis = null;
		FileOutputStream fos = null;
		HSSFWorkbook workbook = null;
		
		try {
			fis = new FileInputStream(ProjectAnalyzerConst.PROJECT_INVENTORY_EXCEL_TEMPLATE);
			workbook = new HSSFWorkbook(fis);
			int excelSheetCount = workbook.getNumberOfSheets();
			
			for (int sheetIdx = 0; sheetIdx < excelSheetCount; sheetIdx++) {
				
				if (sheetIdx == 0) {
					//process project folders
				}
				if (sheetIdx == 1) {
					//process project files
					writeFileDetailsToExcel (workbook.getSheetAt(sheetIdx), files);
				}
				if (sheetIdx == 2) {
					//process project binaries
					writeJarDetailsToExcel (workbook.getSheetAt(sheetIdx), jarDetails); 
				}
			}
			fis.close();
			
			fos = new FileOutputStream(ProjectAnalyzerConst.PROJECT_INVENTORY_EXCEL_TEMPLATE);
			workbook.write(fos);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
				if (fos != null)
					fos.close();
				if (workbook != null)
					workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void writeJarDetailsToExcel (Sheet sheet, List <JarDetails> jarDetails) {
		
		for (int jarCount = 0; jarCount < jarDetails.size(); jarCount++) {
			
			System.out.println("Jar Name:" + jarDetails.get(jarCount).getJarName() + " - Jar Version:" + jarDetails.get(jarCount).getJarVersion());
			
			Row row = createRow (sheet, jarCount + 1);
			Cell cell = createCell (row, 0);
			cell.setCellValue(jarDetails.get(jarCount).getJarName());
			
			cell = createCell (row, 1);
			cell.setCellValue(jarDetails.get(jarCount).getJarVersion());
			
			cell = createCell (row, 2);
			cell.setCellValue(jarDetails.get(jarCount).getJarLocation());
			
			cell = createCell (row, 3);
			cell.setCellValue(jarDetails.get(jarCount).getJarChecksum());
		}
	}
	
	private void  writeFileDetailsToExcel (Sheet sheet, List <File> fileDetails) throws IOException {
		
		for (int fileCount = 0; fileCount < fileDetails.size(); fileCount++) {
						
			Row row = createRow (sheet, fileCount + 1);
			Cell cell = createCell (row, 1);
			cell.setCellValue(fileDetails.get(fileCount).getName());
			
			cell = createCell (row, 2);
			cell.setCellValue(fileDetails.get(fileCount).getCanonicalPath());
		}
	}
	
	private Cell createCell (Row row, int columnIdx) {
		
		Cell cell = row.getCell(columnIdx);
		if (cell == null) {
			cell = row.createCell(columnIdx);
		}
		
		return cell;
	}
	
	private Row createRow (Sheet sheet, int rowIdx) {
		
		Row row = sheet.getRow(rowIdx);
		if (row == null) {
			row = sheet.createRow(rowIdx);
		}
		
		return row;
	}
}
